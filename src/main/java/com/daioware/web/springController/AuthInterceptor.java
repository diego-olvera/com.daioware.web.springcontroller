package com.daioware.web.springController;

import java.lang.reflect.Method;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public abstract class AuthInterceptor<T> extends HandlerInterceptorAdapter{
	
	private Authenticator<T> authenticator;
	
	public AuthInterceptor(Authenticator<T> authenticator) {
		this.authenticator=authenticator;
	}
	public void logRequest(HttpServletRequest request) {
		
	}
	public void logResponse(HttpServletRequest request) {
		
	}
	public void log(String s) {
		System.out.println(s);
	}
	@Override
	public boolean preHandle(
	  HttpServletRequest request,
	  HttpServletResponse response, 
	  Object handler) throws Exception {
		log("Prehandling..., class:"+handler.getClass().getName());
		if(!(handler instanceof HandlerMethod)) {
    		log("Request is not a handler method, returning true");
			return true;
		}
		Method method=((HandlerMethod)handler).getMethod();
		LoginRequired loginAnnotation;
		char accountType;
		T session;
		boolean isValid;
		if((loginAnnotation=method.getDeclaringClass().getAnnotation(LoginRequired.class))!=null
				|| (loginAnnotation=method.getAnnotation(LoginRequired.class))!=null){
        	try {
        		log("Requires log in");
        		log("Checking credentials");
        		logRequest(request);
        		accountType=loginAnnotation.accountType();
        		if(accountType!=LoginRequired.DEFAULT_ACCOUNT_TYPE) {
        			log("Requires specific accountType, checking it...");
    				session=authenticator.getCurrentSession(request);
    				isValid=session!=null?authenticator.isAccountType(session, accountType):false;
        		}
        		else {
        			log("Does not require specific accountType, just checking if there is a session");
        			isValid=authenticator.hasCurrentSession(request);
        		}
    		}catch(Exception e) {
    			isValid= false;
    		}
		}
        else {
        	log("Not Requires log in");
        	isValid=true;
        }
		log("isValid?"+isValid);
		return isValid;
	}

}
