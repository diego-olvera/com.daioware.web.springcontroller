package com.daioware.web.springController;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface LoginRequired {
	char DEFAULT_ACCOUNT_TYPE='0';
	char accountType() default DEFAULT_ACCOUNT_TYPE;
}
