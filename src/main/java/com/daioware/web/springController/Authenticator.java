package com.daioware.web.springController;

import javax.servlet.http.HttpServletRequest;

public abstract class Authenticator<T> {

	public boolean hasCurrentSession(HttpServletRequest request) {
		return getCurrentSession(request)!=null;
	}

	public char getAccountType(HttpServletRequest request) {
		T t=getCurrentSession(request);
		return t!=null?getAccountType(t):'0';
	}
	
	public abstract char getAccountType(T item);

	public boolean isAccountType(T item,char accountType) {
		return getAccountType(item)==accountType;
	}
	public boolean isAccountType(HttpServletRequest req,char accountType) {
		T s=getCurrentSession(req);
		return s!=null?isAccountType(s, accountType):false;
	}
	
	public abstract T getCurrentSession(HttpServletRequest request);
}
